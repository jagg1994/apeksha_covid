<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patientdetails', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('laboratory_no');
            $table->string('sample_no');
            $table->string('send_location');
            $table->string('remark')->nullable();
            $table->string('name')->nullable();
            $table->integer('age')->nullable();
            $table->string('gender')->nullable();
            $table->string('address')->nullable();
            $table->string('occupation')->nullable();
            $table->string('sample_type')->nullable();
            $table->string('institution')->nullable();
            $table->string('ward_icu')->nullable();
            $table->string('bht')->nullable();
            $table->string('location')->nullable();
            $table->integer('date')->nullable();
            $table->string('month')->nullable();
            $table->integer('year')->nullable();
            $table->string('duration_of_illness')->nullable();
            $table->string('temperature')->nullable();
            $table->string('cough')->nullable();
            $table->string('sore_throat')->nullable();
            $table->string('deficulties_in_breathing')->nullable();
            $table->string('diarrhoea')->nullable();
            $table->string('rr')->nullable();
            $table->string('spo2')->nullable();
            $table->string('lung_signs')->nullable();
            $table->string('other_sick')->nullable();
            $table->string('complication')->nullable();
            $table->string('other_complication')->nullable();
            $table->string('wbc')->nullable();
            $table->string('n')->nullable();
            $table->string('l')->nullable();
            $table->string('crp')->nullable();
            $table->string('other_investigations')->nullable();
            $table->string('cxr')->nullable();
            $table->string('other_cxr')->nullable();
            $table->string('co_mobid_conditions')->nullable();
            $table->string('other_co_mobid_conditions')->nullable();
            $table->string('foriegn_travel')->nullable();
            $table->string('country')->nullable();
            $table->string('return_duration')->nullable();
            $table->string('quarantine_details')->nullable();
            $table->string('high_risk_area')->nullable();
            $table->string('risk_area_details')->nullable();
            $table->string('travelto_risk_area')->nullable();
            $table->string('travelto_risk_area_details')->nullable();
            $table->string('contact_with_covid_positive')->nullable();
            $table->string('contact_with_covid_positive_details')->nullable();
            $table->string('contact_with_covid_suspect')->nullable();
            $table->string('contact_with_covid_suspect_details')->nullable();
            $table->string('contact_with_covid_quarantine')->nullable();
            $table->string('contact_with_covid_quarantine_details')->nullable();
            $table->string('contact_with_covid_other')->nullable();
            $table->string('name_of_clinicia')->nullable();
            $table->integer('contact_tel_no')->nullable();
            $table->date('date_of_recieve')->nullable();
            $table->string('in_tripple_package')->nullable();
            $table->string('properly_labled')->nullable();
            $table->string('condition_of_sample')->nullable();
            $table->string('other_condition')->nullable();
            $table->string('results')->nullable();
            $table->string('status')->nullable();
            $table->bigInteger('user')->unsigned()->nullable();
            $table->string('comment')->nullable();
            $table->string('recieved_place')->nullable();
            $table->string('approval1')->nullable();
            $table->string('approval2')->nullable();
            $table->string('approval3')->nullable();
            $table->bigInteger('approval1by')->unsigned()->nullable();
            $table->bigInteger('approval2by')->unsigned()->nullable();
            $table->bigInteger('approval3by')->unsigned()->nullable();
            $table->string('specimen_type')->nullable();
            $table->dateTime('report_date')->nullable();
            $table->string('labstatus')->default("New");
            $table->string('signstatus')->default("Pending");
            $table->string('printstatus')->default("Pending");
            
            $table->string('investigationr')->nullable();
            $table->string('resultsr')->nullable();
            $table->string('comments')->nullable();
            $table->foreign('user')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('approval1by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('approval2by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('approval3by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
  
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patientdetails');
    }
}
