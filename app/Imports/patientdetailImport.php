<?php

namespace App\Imports;
use Illuminate\Support\Facades\Auth;

use App\patientdetail;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;


class patientdetailImport implements ToModel, WithStartRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function startRow(): int
    {
        return 3;
    }
    private $data; 

    public function __construct(array $data = [])
    {
        $this->data = $data; 
    }
    public function model(array $row)
    {
        
        return new patientdetail([
           'laboratory_no' =>  '0',
           'send_location' =>   $this->data['location'],
           'remark' =>   $this->data['remark'],
           'sample_no' =>  $row[0],
           'name' =>  $row[1],
           'age' =>  $row[2],
           'gender' =>  $row[3],
           'address' =>  $row[4],
           'occupation' =>  $row[5],
           'sample_type' =>  $row[6],
           'institution' =>  $row[7],
           'ward_icu' =>  $row[8],
           'bht' =>  $row[9],
           'location'=>  $row[10],
           'date' =>  $row[11],
           'month' =>  $row[12],
           'year' =>  $row[13],
           'duration_of_illness' =>  $row[14],
           'temperature' =>  $row[15],
           'cough' =>  $row[16],
           'sore_throat' =>  $row[17],
           'deficulties_in_breathing' =>  $row[18],
           'diarrhoea' =>  $row[19],
           'rr' =>  $row[20],
           'spo2' =>  $row[21],
           'co_mobid_conditions' =>  $row[22],
           'other_co_mobid_conditions'=>  $row[23],
           'lung_signs' =>  $row[24],
           'other_sick'=>  $row[25],
           'complication' =>  $row[26],
           'other_complication'=>  $row[27],
           'wbc' =>  $row[28],
           'n' =>  $row[29],
           'l' =>  $row[30],
           'crp' =>  $row[31],
           'other_investigations'=>  $row[32],
           'cxr' =>  $row[33],
           'other_cxr'=>  $row[34],
          
           'foriegn_travel'=>  $row[35],
           'country'=>  $row[36],
           'return_duration'=>  $row[37],
           'quarantine_details'=>  $row[38],
           'high_risk_area'=>  $row[39],
           'risk_area_details'=>  $row[40],
           'travelto_risk_area'=>  $row[41],
           'travelto_risk_area_details'=>  $row[42],
           'contact_with_covid_positive'=>  $row[43],
           'contact_with_covid_positive_details'=>  $row[44],
           'contact_with_covid_suspect'=>  $row[45],
           'contact_with_covid_suspect_details'=>  $row[46],
           'contact_with_covid_quarantine'=>  $row[47],
           'contact_with_covid_quarantine_details'=>  $row[48],
           'contact_with_covid_other'=>  $row[49],
           'name_of_clinicia'=>  $row[50],
           'contact_tel_no'=>  $row[51],
           
           'in_tripple_package'=>  $row[53],
           'properly_labled'=>  $row[54],
           'condition_of_sample'=>  $row[55],
           'other_condition'=>  $row[56],
           'results'=>  $row[57],
           'status'=> 'New',
           'user'=>  Auth::user()->id,
           'approval1'=> "pending",
           'approval2'=> "pending",
           'approval3'=> "pending",
           'specimen_type' => "pending",
           'investigationr' => "pending",
           'resultsr' => "pending",
           'comments' => "pending",

        ]);
    }
}
