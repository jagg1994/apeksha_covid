<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\patientdetail;
use App\signedDetails;
use App\resultlist;
use Illuminate\Support\Facades\Auth;
Use \Carbon\Carbon;
use App\Imports\patientdetailImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use PDF;

class patientdetailController extends Controller
{
    public function importExportView()
    {
       return view('pages/import');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        //return Excel::download(new UsersExport, 'users.xlsx');
    }
   
    /**
    * @return \Illuminate\Support\Collection
    */

    public function viewalldata(){
        $empp = DB::table('patientdetails')
        ->select('patientdetails.*')        
        ->where('status','!=', "Printed")
        ->orderBy('id', 'DESC')
        ->get();
        return view('pages/view_patientall')->with('patient',$empp)->with('request',"");
    }

    public function patientform($id){
        $empp = DB::table('patientdetails')
        ->select('patientdetails.*')
        ->where('patientdetails.id','=', $id)
        ->get();
        return view('pages/patientform')->with('patient',$empp);
    }

    public function changelabno(Request $request){
        $patient = patientdetail::find($request->id);
       
        $patient->laboratory_no = $request->laboratory_no;
        $patient->status = "Testing";
        $patient->update();
        return back();
    }
    public function changelabdetails(Request $request){
        $patient = patientdetail::find($request->id);
       
        $patient->in_tripple_package = $request->tripple;
        $patient->properly_labled = $request->labled;
        $patient->condition_of_sample = $request->condition;
        $patient->other_condition = $request->other_condition;
        $patient->results = $request->result;

        $patient->status = "Await Sign";
        $patient->update();
        return back();
    }
    
    public function import(Request $request) 
    {
        $data = [
            'location' => $request->location, 
            'remark' => $request->remark,
            // other data here
        ];
        Excel::import(new patientdetailImport($data),request()->file('file'));
           
        return view('pages/import')->with('message','Successfully Entered!');
    }


    public function resultform($id){
        $empp = DB::table('patientdetails')
        ->select('patientdetails.*')
        ->where('patientdetails.id','=', $id)
        ->get();
        return view('pages/resultform')->with('patient',$empp);
    }

    public function  resultsubmit(Request $request){
        $patient = patientdetail::find($request->id);
        $patient->name = $request->name;
        $patient->age = $request->age;
        $patient->gender = $request->gender;
        $patient->bht = $request->bht;
        $patient->investigationr = $request->investigation;
        $patient->resultsr = $request->result;
        $patient->specimen_type = $request->specimen_type;
        $patient->sample_type = $request->specimen_type;
        $patient->comments = $request->comments;
     
    
        $patient->update();
        return back();
    }
    
    public function signreport(Request $request){
        $patient = patientdetail::find($request->id);
        
        if($request->first=="first"){
            $patient->approval1 = "Approved";
            $patient->approval1by = Auth::user()->id;
            $patient->signstatus = "Waiting for review MLT sign";
            $singned = new signedDetails;
            $singned->user = Auth::user()->id;
            $singned->report = $request->id;
            $singned->type = "MLT First";
            $patient->update();
            $singned->save();
            

        }
        if($request->second=="second"){
            $patient->approval2 = "Approved";
            $patient->approval2by = Auth::user()->id;
            $patient->signstatus = "Waiting for Consultant Sign";
            $singned = new signedDetails;
            $singned->user = Auth::user()->id;
            $singned->report = $request->id;
            $singned->type = "MLT review";
            $patient->update();
            $singned->save();
            

        }
        if($request->third=="third"){
            $patient->approval3 = "Approved";
            $patient->approval3by = Auth::user()->id;
            $patient->signstatus = "Signed Complete";
            $patient->report_date = Carbon::now();
            $singned = new signedDetails;
            $singned->user = Auth::user()->id;
            $singned->report = $request->id;
            $singned->type = "Consultant";
            $patient->update();
            $singned->save();
            

        }
        return back();
    }
    public function filterData(Request $request){
        $from = $request->from_date;
        $to = $request->to_date;
        $lab = $request->lab;
        $sign = $request->sign;
        if($lab=="All"){
            if($sign=="All"){
                $empp = DB::table('patientdetails')
                ->select('patientdetails.*')
                ->whereBetween('created_at',[$from, $to])
                ->get();
            }else{
                $empp = DB::table('patientdetails')
                ->select('patientdetails.*')
                ->whereBetween('created_at',[$from, $to])
                ->where('signstatus','=', $sign)
                ->get();
            }
        }else{
            if($sign=="All"){
                $empp = DB::table('patientdetails')
                ->select('patientdetails.*')
                ->whereBetween('created_at',[$from, $to])
                ->where('status','=', $lab)
                ->get();
            }else{
                $empp = DB::table('patientdetails')
                ->select('patientdetails.*')
                ->whereBetween('created_at',[$from, $to])
                ->where('status','=', $lab)
                ->where('signstatus','=', $sign)
                ->get();
            }
        }
        
        return view('pages/view_patientall')->with('patient',$empp)->with('request',$request);

    }
    
    public function printDatap(Request $request){
        $from = $request->from_date;
        $to = $request->to_date;
        
        
         $empp = DB::table('patientdetails')
                ->select('patientdetails.*')
                ->whereBetween('created_at',[$from, $to])
                ->where('approval3','!=', "pending")
                ->where('status','!=', "Printed")
                ->orderBy('id', 'DESC')
                ->get();
        
        return view('pages/printing_data')->with('patient',$empp)->with('request',$request);

    }

    public function printData(){
      
         $empp = DB::table('patientdetails')
                ->select('patientdetails.*')
                ->where('approval3','!=', "pending")
                ->where('status','!=', "Printed")
                ->orderBy('id', 'DESC')
                ->get();
        
        return view('pages/printing_data')->with('patient',$empp)->with('request',"");

    }
    public function printlist(){
      
        $empp = DB::table('patientdetails')
        ->select('patientdetails.*')
        ->where('created_at', Carbon::now())
        ->where('approval3','!=', "pending")
        ->where('status','!=', "Printed")
        ->orderBy('id', 'DESC')
        ->get();
       return view('pages/printing_list')->with('patient',$empp)->with('request',"")->with('list',"");

   }

   public function printlistp(Request $request){
    $from = $request->from_date;
    $location= $request->location;
    if($location==""){
        $empp = DB::table('patientdetails')
        ->select('patientdetails.*')
        ->whereDate('created_at','=',$from)
        ->orderBy('id', 'DESC')
        ->where('approval3','!=', "pending")
        ->get();
    }else{
        $empp = DB::table('patientdetails')
        ->select('patientdetails.*')
        ->whereDate('created_at','=',$from)
        ->where(function($q) use ($location) {
            $q->where('send_location','=',  $location)
              ->orWhere('location','=',$location);
        })
        ->where('approval3','!=', "pending")
        ->orderBy('id', 'DESC')
        ->get();

    }
   
    return view('pages/printing_list')->with('patient',$empp)->with('request',$request);



}

    public function printedDatap(Request $request){
        $from = $request->from_date;
        $to = $request->to_date;
        
        
         $empp = DB::table('patientdetails')
                ->select('patientdetails.*')
                ->whereBetween('created_at',[$from, $to])
                ->where('status','=', "Printed")
                ->orderBy('id', 'DESC')
                ->get();
        
        return view('pages/printed_data')->with('patient',$empp)->with('request',$request);

    }

    public function printedData(){
      
         $empp = DB::table('patientdetails')
                ->select('patientdetails.*')
                ->where('status','=', "Printed")
                ->orderBy('id', 'DESC')
                ->get();
        
        return view('pages/printed_data')->with('patient',$empp)->with('request',"");

    }
    public function downloadPDF($id){
        $patient= DB::table('patientdetails')
        ->select('patientdetails.*')
        ->where('patientdetails.id','=', $id)
        ->get();

        $pdf = PDF::loadView('pages/pdf', compact('patient'));
        return $pdf->download('invoice.pdf');
    }
    public function savePrinted($id){
        $patient = patientdetail::find($id);
        $patient->status = "Printed";
        $patient->update();
        return back();
    }
    
}
