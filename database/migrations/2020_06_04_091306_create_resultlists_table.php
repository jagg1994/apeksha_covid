<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resultlists', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('location');
            $table->dateTime('recieved_date')->nullable();
            $table->string('approval1')->nullable();
            $table->string('approval2')->nullable();
            $table->string('approval3')->nullable();
            $table->bigInteger('approval1by')->unsigned()->nullable();
            $table->bigInteger('approval2by')->unsigned()->nullable();
            $table->bigInteger('approval3by')->unsigned()->nullable();
            
            $table->foreign('approval1by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('approval2by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('approval3by')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resultlists');
    }
}
