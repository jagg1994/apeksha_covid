@extends('layouts.app')
<style>
    table p{
        margin-bottom: 2px;
    }
</style>

@section('content')

<div class="container">
    <div class="row justify-content-center">
	
		<div class="col-md-offset-2 col-md-12 col-lg-offset-3 col-lg-12">
            @foreach($patient as $patients)
    <div class="card">
        <div class="card-body">
    	    <div class="well profile">
            
            <div class="col-sm-12">
                @if($patients->laboratory_no == "0")
                <div class="col-xs-3 col-sm-3" style="float: left;">
                    <p>dbhdhs</p>
                    <form method="post" action="/changeLabNo" class="form-horizontal">
                        @csrf
                     
                    <lable><strong>NCI Lab No</strong> </lable>
                        <div class="row">
                        <input type="text" class="form-control col-sm-6"  value="{{$patients->laboratory_no}}" name="laboratory_no">
                        <input type="hidden"   value="{{$patients->id}}" name="id">
                         <button href="#" type="submit" class="btn btn-warning">Change</button>
                         <lable><strong>Collection Center Assigned No: {{$patients->sample_no}}</strong></lable>    
                  
                        </div>
                    </form>
                </div>
                
                <br> <br>
                
                
                
                <div class="col-xs-9 col-sm-9" style="float: left;" >
                    <h4 style="text-align: center">Request form - SARS CoV-2 real time RT-PCR <br>
                        Virology Laboratory - Apeksha Hospital Maharagama
                      </h4>
                </div>
                @endif
                @if($patients->laboratory_no != "0")
               
                <div class="col-xs-3 col-sm-3" style="float: left; width:100%">
                    
                  
                    <lable><strong>NCI Lab No: {{$patients->laboratory_no}}</strong></lable>
                    <br>
                    <lable><strong>Collection Center Assigned No: {{$patients->sample_no}}</strong></lable>    
                  
                </div>
                
                <div class="col-xs-3 col-sm-3" style="float: right; width:100%">
                    @if($patients->status != 'pending labdetails')
                    <a href="/resultForm/{{$patients->id}}" class="btn btn-primary" style="float: right;">Add Results</a>
                    @endif
                   
                </div>
                
                <div class="col-xs-12 col-sm-12" style="float: right;" >
                    <h4 style="text-align: center">Request form - SARS CoV-2 real time RT-PCR <br>
                        Virology Laboratory - Apeksha Hospital Maharagama
                      </h4>
                </div>
               
                @endif

                <br>  <br><br>  <br>
                <div class="col-xs-6 col-sm-6" style="float: left;width:100%;">
                    
                  
                    <p><strong>Name: </strong> {{$patients->name}} </p>
                    <p><strong>Gender: </strong> {{$patients->gender}} </p>
                    <p><strong>BHT No: </strong> {{$patients->bht}} </p>
                    <p><strong>Address: </strong> {{$patients->address}} </p>
                   
                </div>
                <div class="col-xs-6 col-sm-6" style="float: left;width:100%;">
                    
                    
                    <p><strong>Age: </strong> {{$patients->age}} </p>
                    <p><strong>Ward/ICU: </strong> {{$patients->ward_icu}} </p>
                    <p><strong>Institution: </strong> {{$patients->institution}} </p>
                    <p><strong>Occupation: </strong> {{$patients->occupation}} </p>
                   
                    
                </div>
               
            </div> 
          
            </div>
            </div> 
    </div>
            <div class="card" >
                <div class="card-body">
                    <div class="well profile">
                        <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                        <p><strong>Sample Type: </strong> {{$patients->sample_type}} </p>
                        <p><strong>Date of Collection: </strong> {{$patients->date}}-{{$patients->month}}-{{$patients->year}}  </p>
                        </div>
                    </div>
                </div>
              </div> 
              <div class="card" >
                <div class="card-body">
                    <div class="well profile">
                        <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                        <p><strong>Clinical History: </strong> Duration of illness {{$patients->duration_of_illness}} days</p>
                        
                        </div>
                        <div class="col-sm-4" style="float: left;width:100%;">
                            <table class="table table-bordered" style="font-size: 0.9rem;">
                            <tbody>
                                <tr>
                                 <td style="max-width:100px;">  <p><strong>Temperature>38C: </strong> </p></td>
                                 <td> <p>{{$patients->temperature}}  </p>
                                   
                                 </td>
                                </tr> 
                                <tr>
                                 <td style="max-width:100px;">  <p><strong>Cough:</strong> </p></td>
                                 <td> <p>{{$patients->cough}}  </p>
                                   
                                 </td>
                                </tr> 
                                <tr>
                                 <td style="max-width:100px;">  <p><strong>Sore throat:</strong> </p></td>
                                 <td> <p> {{$patients->sore_throat}} </p>
                                   
                                 </td>
                                </tr> 
                                <tr>
                                 <td style="max-width:100px;">  <p><strong>Difficulty in breathing/SOB:</strong> </p></td>
                                 <td> <p>{{$patients->deficulties_in_breathing}} </p>
                                   
                                 </td>
                                </tr> 
                                <tr>
                                 <td style="max-width:100px;">  <p><strong>Diarrhea:</strong> </p></td>
                                 <td> <p>{{$patients->diarrhoea}} </p>
                                   
                                 </td>
                                </tr> 
                                <tr>
                                 <td style="max-width:100px;">  <p><strong>RR > 30: </strong> </p></td>
                                 <td> <p>{{$patients->rr}} </p>
                                   
                                 </td>
                                </tr> 
                                <tr>
                                 <td style="max-width:100px;">  <p><strong>SpO2 < 90 on room air:</strong> </p></td>
                                 <td> <p>{{$patients->spo2}} </p>
                                   
                                 </td>
                                 </tr> 
                                </tbody>
                            </table>
                    
                         
                            
                        </div>
                        <div class=" col-sm-8" style="float: right;width:100%;">
                            
                            <table class="table table-bordered" style="font-size: 0.9rem;">
                                <tbody>
                                   <tr>
                                    <td style="max-width:40px;"> <p><strong>Lung signs: </strong> </p></td>
                                    <td> <p><strong>{{$patients->lung_signs}}</strong> </p>
                                        <p><strong>Other: </strong>{{$patients->other_sick}} </p>
                                    </td>
                                    </tr> 
                                    <tr>
                                    <td style="max-width:50px;"> <p><strong>Complications: </strong> </p></td>
                                    <td> <p><strong>{{$patients->complication}}</strong> </p>
                                        <p><strong>Other: </strong>{{$patients->other_complication}} </p>
                                    </td>
                                    </tr> 
                                    <tr>
                                        <td style="max-width:50px;"> <p><strong>Investigations: </strong> </p></td>
                                        <td> <p><strong>WBC</strong> {{$patients->wbc}}</strong>
                                            <strong>N</strong> {{$patients->n}}</strong> 
                                            <strong>L</strong> {{$patients->l}}</strong> 
                                            <strong>CRP</strong> {{$patients->crp}}</strong> 
                                        </p>
                                            <p><strong>Other: </strong>{{$patients->other_investigations}} </p>
                                        </td>
                                        </tr> 
                                        <tr>
                                            <td style="max-width:50px;"> <p><strong>CXR: </strong> </p></td>
                                            <td> <p><strong>{{$patients->cxr}}</strong> </p>
                                                <p><strong>Other: </strong>{{$patients->other_cxr}} </p>
                                            </td>
                                            </tr> 
                                        </tbody>
                                </table>
             
                            
                        </div>
                        <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                            <p><strong>Co-morbid conditions:</strong> {{$patients->co_mobid_conditions}}</p>
                            <p><strong>Other Co-morbid conditions:</strong> {{$patients->other_co_mobid_conditions}}</p>
                            </div>
                    </div>
                </div>
              </div>  
              <div class="card" >
                <div class="card-body">
                    <div class="well profile">
                        <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                            <h6><strong>Epidemiological History</strong></h6>
                       </div>
                       
                        <div class=" col-sm-12" style="width:100%;">
                            
                            <table class="table table-bordered" style="font-size: 0.9rem;">
                                <tbody>
                                   <tr>
                                    <td style="max-width:70px;"> <p><strong>Foreign travel history: </strong> </p></td>
                                    <td> <p><strong>{{$patients->foriegn_travel}} &nbsp;&nbsp;&nbsp;&nbsp; Country:</strong>{{$patients->country}}</p>
                                        <p><strong>Duration since return:</strong>{{$patients->return_duration}} </p>
                                        <p><strong>Quarantine details::</strong>{{$patients->quarantine_details}} </p>
                                    </td>
                                    </tr> 
                                    <tr>
                                    <td style="max-width:70px;"> <p><strong>Residing in a high-risk area: </strong> </p></td>
                                    <td> <p><strong>{{$patients->high_risk_area}}</strong> </p>
                                        <p><strong>Details: </strong>{{$patients->risk_area_details}} </p>
                                    </td>
                                    </tr> 
                                    <tr>
                                        <td style="max-width:70px;"> <p><strong>Travel to high risk areas: </strong> </p></td>
                                        <td> <p><strong>{{$patients->travelto_risk_area}}</strong> </p>
                                            <p><strong>Details: </strong>{{$patients->travelto_risk_area_details}} </p>
                                        </td>
                                        </tr> 
                                        <tr>
                                            <td style="max-width:70px;"> <p><strong>Contact with a COVID positive patient </strong> </p></td>
                                            <td> <p><strong>{{$patients->contact_with_covid_positive}}</strong> </p>
                                                <p><strong>Details: </strong>{{$patients->contact_with_covid_positive_details}} </p>
                                            </td>
                                            </tr> 
                                            <tr>
                                                <td style="max-width:70px;"> <p><strong>Contact with a COVID suspected patient </strong> </p></td>
                                                <td> <p><strong>{{$patients->contact_with_covid_suspect}}</strong> </p>
                                                    <p><strong>Details: </strong>{{$patients->contact_with_covid_suspect_details}} </p>
                                                </td>
                                                </tr> 
                                                <tr>
                                                    <td style="max-width:70px;"> <p><strong>Contact with quarantined patient</strong> </p></td>
                                                    <td> <p><strong>{{$patients->contact_with_covid_quarantine}}</strong> </p>
                                                        <p><strong>Details: </strong>{{$patients->contact_with_covid_quarantine_details}} </p>
                                                    </td>
                                                    </tr> 
                                                    <tr>
                                                        <td style="max-width:70px;"> <p><strong>Other</strong> </p></td>
                                                        <td> <p><strong>{{$patients->contact_with_covid_other}}</strong> </p>
                                                            </td>
                                                        </tr> 
                                        </tbody>
                                </table>
             
                            
                        </div>
                       
                    </div>
                </div>
              </div>  
              <div class="card" >
                <div class="card-body">
                    <div class="well profile">
                        <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                            <p><strong>Name of the Clinician/HO/SHO: </strong> {{$patients->name_of_clinicia}} </p>
                            <p><strong>Contact telephone number (Mandatory): </strong> {{$patients->contact_tel_no}} </p>
                            </div>
                    </div>
               
                    <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                        <hr class="hr">
                        <p> <strong>Note:</strong> the request may be rejected if this form is not filled properly & the specimen is not transferred properly. It will take 24-48 hours
                to obtain the results after specimen reached the laboratory.
                        </p>
                        <hr class="hr">
            </div>
                </div>
              </div> 

              <div class="card" >
                <div class="card-body">
                <div class="well profile" id="lab_only">

                        <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                            <h5><strong>FOR LABORATORY PURPOSE ONLY </strong></h5>
                         </div>
               
                    <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                        <div class="col-xs-4 col-sm-4" style="float: left;width:100%;">
                            <p><strong>Date of sample receive: </strong> {{$patients->created_at}} </p>
                            
                        </div>
                        <div class="col-xs-4 col-sm-4" style="float: left;width:100%;">
                            <p><strong>In triple package: </strong> {{$patients->in_tripple_package}} </p>
                            
                        </div>
                        <div class="col-xs-4 col-sm-4" style="float: left;width:100%;">
                            <p><strong>Properly labeled: </strong> {{$patients->properly_labled}} </p>
                            
                        </div>
                        <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                            <p><strong>Condition of the sample: </strong> {{$patients->condition_of_sample}} </p>
                            <p><strong>Other Conditions: </strong> {{$patients->other_condition}} </p>
                           
                        </div>
                        <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                            <p><strong>Results: </strong> {{$patients->results}} </p>
                            @if($patients->status != 'new')
                            @if($patients->approval1 == "pending")
                            <button onclick="editView()" type="button" class="btn btn-warning">Change Data</button>
                            @endif
                            @endif
                        </div>

                        
                    </div>
                </div>

                <div class="well profile" id="lab_only_edit" style="display: none">
                    <form method="post" action="/changeLabDetails" class="form-horizontal">
                        @csrf
                     
                    <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                        <h5><strong>FOR LABORATORY PURPOSE ONLY </strong></h5>
                     </div>
           
                <div class="col-xs-12 col-sm-12" style="float: left;width:100%;">
                    <div class="col-xs-4 col-sm-4" style="float: left;width:100%;">
                        <p><strong>Date of sample receive: </strong> {{$patients->date_of_recieve}} </p>
                        
                    </div>
                    <div class="col-xs-4 col-sm-4 row" style="float: left;width:100%;">
                        <lable class="col-sm-6"><strong>In triple package: </strong></lable>
                        <select class="form-control col-sm-3" name="tripple">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    
                    </div>
                    <div class="col-xs-4 col-sm-4 row" style="float: left;width:100%;">
                        <lable class="col-sm-6"><strong>Properly labeled:  </strong></lable>
                        <select class="form-control col-sm-3" name="labled">
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                        
                    </div>
                    <div class="col-xs-12 col-sm-12 row" style="float: left;width:100%; margin-top: 5px;">
                        <lable class="col-sm-3"><strong>Condition of the sample: </strong></lable>
                        <select class="form-control col-sm-3" name="condition">
                            <option value=""></option>
                            <option value="leakng">Leaking</option>
                            <option value="not labled">Not Labled</option>
                            <option value="other">Other</option>
                        </select>
                       
                    </div>
                    <div class="col-xs-12 col-sm-12 row" style="float: left;width:100%; margin-top: 5px;">
                        <lable class="col-sm-3"><strong>Other Conditions: </strong></lable>
                        <input type="text" class="form-control col-sm-6" name="other_condition" value="{{$patients->other_condition}}">
                        <input type="hidden" class="form-control col-sm-6" name="id" value="{{$patients->id}}">
                    </div>
                    <div class="col-xs-12 col-sm-12 row" style="float: left;width:100%; margin-top: 5px;">
                        <lable class="col-sm-3"><strong>Results:</strong></lable>
                        <select class="form-control col-sm-3" name="result">
                            <option value=""></option>
                            <option value="positive">Positive</option>
                            <option value="negative">Negative</option>
                        </select>
                        </div>
                    <div class="col-xs-12 col-sm-12row" style="float: left;width:100%; margin-top: 5px;">
                        
                        <button  type="submit" class="btn btn-success">Change</button>
                        <button  onclick="backView()" type="button" class="btn btn-primary">Go Back</button>
                    </div>

                    
                </div>
                    </form>
            </div>

              </div> 
              @endforeach  
		
      
    </div>      
</div>
</div>           

@endsection

<script>
    function editView(){
        $('#lab_only_edit').show();
        $('#lab_only').hide();

    }
    function  backView(){
        $('#lab_only_edit').hide();
        $('#lab_only').show();

    }
   
</script>