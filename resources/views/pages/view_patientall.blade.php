@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="filterData" class="form-horizontal">
                                   
                        @csrf
                     <div class="row">
                        
                        <a href="importExportView" style="height: 50%; margin-left: 10px;" type="button" class="btn btn-primary" >{{ __('Import New Data Set') }}</a>
                       
                        
                      </div>     
                      <br>
                      <div class="row" style="margin-left: 10px">

                          
                        <label class="col-form-label">{{ __('From:') }}</label>
                        <div class="col-sm-2">
                            @if($request!="")
                            <input class="date form-control" value="{{$request->from_date}}" required name="from_date" id="from_date" type="date"  />
                           
                            @endif
                            @if($request=="")
                            <input class="date form-control"  required name="from_date" id="from_date" type="date"  />
                           
                            @endif

                            
                        </div>
                        <label class="col-form-label">{{ __('To:') }}</label>
                        
                        <div class="col-sm-2">
                            @if($request!="")
                            <input class="datepicker form-control" value="{{$request->to_date}}" required name="to_date" id="to_date" type="date"  />
                            @endif
                            @if($request=="")
                            <input class="datepicker form-control"  required name="to_date" id="to_date" type="date"  />
                           
                            @endif
                        </div>
                        
                        <label class="col-form-label">{{ __('Lab Status:') }}</label>
                        <div class="col-sm-2">
                         
                            <select id="lab"  class="form-control col-sm-10" name="lab" >
                                @php
                                $selected1=""; $selected2=""; $selected3=""; $selected4="";
                                @endphp
                                @if($request!="")
                                @php
                                if($request->lab=="All"){
                                    $selected1 = "selected";
                                }
                                if($request->lab=="New"){
                                    $selected2 = "selected";
                                }
                                if($request->lab=="Testing"){
                                    $selected3 = "selected";
                                }
                                if($request->lab=="Await Sign"){
                                    $selected4 = "selected";
                                }
                                
                                @endphp
                                @endif
                                <option {{$selected1}} value="All">All</option>
                                <option {{$selected2}} value="New">New</option>
                                <option {{$selected3}} value="Testing">Testing</option>
                                <option {{$selected4}} value="Await Sign">Await Sign</option>
                               
                            </select>
                           
                        </div>
                        <label class="col-form-label">{{ __('Sign Status:') }}</label>
                        
                        <div class="col-sm-2">
                         
                            <select id="sign"  class="form-control col-sm-10" name="sign">
                                @php
                                $selected5=""; $selected6=""; $selected7=""; $selected8="";$selected9="";
                                @endphp
                                @if($request!="")
                                @php
                                if($request->sign=="All"){
                                    $selected5 = "selected";
                                }
                                if($request->sign=="Pending"){
                                    $selected6 = "selected";
                                }
                                if($request->sign=="Waiting for review MLT sign"){
                                    $selected7 = "selected";
                                }
                                if($request->sign=="Waiting for Consultant Sign"){
                                    $selected8 = "selected";
                                }
                                if($request->sign=="Signed Complete"){
                                    $selected9 = "selected";
                                }
                                
                                @endphp
                                @endif

                                <option {{$selected5}} value="All">All</option>
                                <option {{$selected6}} value="Pending">Pending</option>
                                <option {{$selected7}} value="Waiting for review MLT sign">Waiting for review MLT sign</option>
                                <option {{$selected8}} value="Waiting for Consultant Sign">Waiting for Consultant Sign</option>
                                <option {{$selected9}} value="Signed Complete">Signed Complete</option>
                               
                            </select>
                           
                        </div>
                        <div class="col-sm-1">
                            <button type="submit" name="filter" class="btn btn-warning">Filter</button>
                        </div>
                        
                      </div> 
                    </form>
                      <br>
                   
                      <table class="table table-striped table-bordered" style="width:100%" id="table">
                        <thead>
                            <tr>
                                <th class="text-center" style="max-width: 50px">NCI Lab No</th>
                                <th class="text-center" style="max-width: 50px">CCA No</th>
                               
                                <th class="text-center">Name</th>
                                
                                <th class="text-center">Sample Type</th>
                                
                               <th class="text-center">Recieved Date</th>
                                
                                <th class="text-center">Lab Status</th>
                             
                                <th class="text-center">Sign Status</th>
                                
                                <th class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($patient as $patients)
                            <tr>
                                <td>{{$patients->laboratory_no}}</td>
                                <td>{{$patients->sample_no}}</td>
                                <td>{{ $patients->name}}</td>
                                
                                <td>{{$patients->sample_type}}</td>
                                <td>{{$patients->created_at}}</td>
                                @php $colors="teal";@endphp
                                @if($patients->status=="New")
                                @php $colors="teal";@endphp
                                @endif
                                @if($patients->status=="Testing")
                                    @php $colors="sienna";@endphp
                                @endif
                                @if($patients->status=="Await Sign")
                                @php $colors="olive" ;
                                @endphp
                                @endif
                                     
                              
                                <td style="text-align: center"><span class="btn" style="color:white; padding:0; padding-left: 1px; padding-right: 1px; min-width: 100px; background-color: {{$colors}}">{{$patients->status}}</span></td>
                                
                                
                                @if($patients->signstatus=="Pending")
                                @php $color="blue" ;@endphp
                                @endif
                                @if($patients->signstatus=="Signed Complete")
                                    @php $color="green" ;@endphp
                                @endif
                                @if($patients->signstatus=="Waiting for review MLT sign")
                                @php $color="darkcyan" ;@endphp
                                @endif
                                @if($patients->signstatus=="Waiting for Consultant Sign")
                                @php $color="slateblue" ;@endphp
                                @endif                              

                                <td style="text-align: center"><span class="btn" style="color:white; padding:0; padding-left: 1px; padding-right: 1px; background-color: {{$color}}">{{$patients->signstatus}}</span></td>
                               
                                <td style="text-align: center">
                                  
                                   
                                    <a href="patientForm/{{$patients->id}}" type="button" class="btn  btn-sm btn-success" >{{ __('View') }}</a>
                              
                                   
                                    
                                </td>
                            </tr>
                           
                           
                            @endforeach
                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>
</div> 
       
<script>
    {{-- $('#lab').change(function() {
        alert($("#lab :selected").attr('value'))
    }); --}}

   $(document).ready(function() {
        
      $('#table').DataTable({
        "paging":   false,
        "ordering": false,
        "scrollY":  "500px",
        "scrollX": true,
        "scrollCollapse": true
        
    });
   
  });

 
   </script>
     

<script>
    function deleteConfirm(id) {
        var txt;
        if (confirm("Are you sure you want to delete this!")) {
            window.location.href='deletepatients/'+id;
        } else {
         
        }
        document.getElementById("demo").innerHTML = txt;

       
      }
</script>

@endsection

