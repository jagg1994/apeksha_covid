<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignedDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signed_details', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->bigInteger('user')->unsigned()->nullable();
            $table->bigInteger('report')->unsigned()->nullable();
            $table->string('type');
            $table->foreign('user')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('report')->references('id')->on('patientdetails')->onDelete('cascade')->onUpdate('cascade');
            


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signed_details');
    }
}
