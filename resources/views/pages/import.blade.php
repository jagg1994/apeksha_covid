@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

<div class="card">

    <div class="card-header">

        Add New Data
    </div>

    <div class="card-body">

        <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">

            @csrf

            <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Recieved Location') }}</label>
                <div class="col-sm-3">
                  <div class="form-group">
                    <input class="form-control" name="location" id="input-location" type="text" placeholder="{{ __('Recieved Location') }}" required="true" aria-required="true"/>
                    
                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Remark') }}</label>
                <div class="col-sm-10">
                  <div class="form-group">
                    <input class="form-control" name="remark" id="input-remark" type="text" placeholder="{{ __('Remark') }}"/>
                    
                  </div>
                </div>
              </div>
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Upload the file') }}</label>
                <div class="col-sm-10">
                  <div class="form-group">
                    <input type="file" name="file" class="form-control"  required="true"> 
                  </div>
                </div>
              </div>

           

            <br>

            <button class="btn btn-success">Save Patient Data</button>

           
            </form>
            <br>
            @if ($message ?? '')
            <div class="alert alert-success" role="alert">
                {{ $message ?? '' }}
            </div>
        @endif

            </div>

        </div>

        </div>
    </div>
</div>
@endsection