<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::group( ['middleware' => 'auth' ], function()
{
//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'patientdetailController@viewalldata')->name('home');
Route::get('/map', 'employeeController@mapmain')->name('employee_map');
//Route::get('/', 'employeeController@mapmain');
Route::get('/', 'patientdetailController@viewalldata');

Route::get('profile/{id}', 'profileController@view')->name('profileView');
Route::get('export', 'patientdetailController@export')->name('export');
Route::get('viewalldata', 'patientdetailController@viewalldata')->name('viewalldata');
Route::get('importExportView', 'patientdetailController@importExportView');
Route::post('import', 'patientdetailController@import')->name('import');
Route::get('patientForm/{id}', 'patientdetailController@patientform')->name('patientForm');
Route::post('/changeLabNo', 'patientdetailController@changelabno')->name('changeLabNo');
Route::post('/changeLabDetails', 'patientdetailController@changelabdetails')->name('changeLabDetails');
Route::get('resultForm/{id}', 'patientdetailController@resultform')->name('resultForm');
Route::post('/resultsubmit', 'patientdetailController@resultsubmit')->name('resultsubmit');
Route::post('/signReport', 'patientdetailController@signreport')->name('signReport');
Route::post('/filterData', 'patientdetailController@filterData')->name('filterData');
Route::get('/filterData', 'patientdetailController@viewalldata')->name('filterData');

Route::get('/downloadPDF/{id}','patientdetailController@downloadPDF');
Route::get('/savePrinted/{id}','patientdetailController@savePrinted');

Route::post('/printData', 'patientdetailController@printDatap')->name('printDatap');
Route::get('/printData', 'patientdetailController@printData')->name('printData');

Route::post('/printlist', 'patientdetailController@printlistp')->name('printlistp');
Route::get('/printlist', 'patientdetailController@printlist')->name('printlist');

Route::post('/printedData', 'patientdetailController@printedDatap')->name('printedDatap');
Route::get('/printedData', 'patientdetailController@printedData')->name('printedData');




});