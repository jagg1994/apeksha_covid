<!doctype html>
<html >
<head>
   
</head>
    <body>
        <div class="container">
            <div class="row justify-content-center">
            
                <div class="col-md-offset-2 col-md-12 col-lg-offset-3 col-lg-12">
                    @foreach($patient as $patients)
                         <div class="card">
                            <div class="card-body">
                                <div class="well profile">
                    
                                    <div class="col-sm-12">
                                        
                                        <div class="row" style="margin-top: 50px;">
                                            <div class="col-xs-2 col-sm-2" style="padding-left: 70px">
                                                <img src=" {{ asset('img/glogo.jpg') }}" style="height: 80px"/>
                                                   
                                            </div>
                                            <div class="col-xs-10 col-sm-10" style="text-align: center">
                                               <h2>APEKSHA HOSPITAL(MAHARAGAMA)-SRILANKA</h2> 
                                               <h2>DEPARTMENT OF VIROLOGY</h2> 
                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-xs-11 col-sm-11" style="text-align: center">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <p><strong>Telephone: 2844450</strong></p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p><strong>Ex: 1248 </strong></p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p><strong>Fax: 2842051</strong></p>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <p><strong>Web: www.ncisl.lk</strong></p>
                                                    </div>
                                                </div> 
                                             </div>
                                             <div class="col-sm-1"></div>
        
                                             <div class="col-xs-11 col-sm-11" style="text-align: center">
                                                <h3>CONFIDENTIAL LABORATORY REPORT</h3> 
                                               
                                             </div>
                                             <div class="col-xs-12 col-sm-12" style="text-align: center; margin-bottom: 20px;">
                                                <hr style="height:2px;border-width:0;color:gray;background-color:gray">
                                               
                                             </div>
                                                
                                        
                                            </div>
                                        
                                        <div class="row">
                                            <div class="col-sm-1"></div>
                                            <div class="col-xs-6 col-sm-6" >
                                                <div class="form-group row">
                                                    <lable class="col-sm-4"><strong>NCI Lab No  </strong></lable>
                                                    <div class="col-xs-8 col-sm-8" >
                                                        <p>: {{$patients->laboratory_no}}</p>
                                                    </div>
                                                    <lable class="col-sm-4"><strong>Collection Center Assigned No  </strong></lable>
                                                    <div class="col-xs-8 col-sm-8" >
                                                        <p>: {{$patients->sample_no}}</p>
                                                    </div>
                                                     </div>
                                            </div>
                                            <div class="col-xs-5 col-sm-5" >
                                                <div class="form-group row">
                                                    <lable class="col-sm-4"><strong>Specimen </strong></lable>
                                                    <div class="col-xs-8 col-sm-8" >
                                                        <p id="p-type">: {{$patients->specimen_type}}</p>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
        
                                        <div class="row">
                                            <div class="col-sm-1"></div>
                                            <div class="col-xs-6 col-sm-6" >
                                                <div class="form-group row">
                                                    <lable class="col-sm-4"><strong>Name  </strong></lable>
                                                    <div class="col-xs-8 col-sm-8" >
                                                        <p>: {{$patients->name}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-5 col-sm-5" >
                                                <div class="form-group row">
                                                    <lable class="col-sm-4"><strong>Received Date  </strong></lable>
                                                    <div class="col-xs-8 col-sm-8" >
                                                        <p>: {{$patients->created_at}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
        
                                        <div class="row">
                                            <div class="col-sm-1"></div>
                                            <div class="col-xs-6 col-sm-6" >
                                                <div class="form-group row">
                                                    <lable class="col-sm-4"><strong>Age</strong></lable>
                                                    <div class="col-xs-8 col-sm-8" >
                                                        <p>: {{$patients->age}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-5 col-sm-5" >
                                                <div class="form-group row">
                                                    <lable class="col-sm-4"><strong>Reporte Date  </strong></lable>
                                                    <div class="col-xs-8 col-sm-8" >
                                                        <p>: {{$patients->report_date}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
        
                                        <div class="row">
                                            <div class="col-sm-1"></div>
                                            <div class="col-xs-6 col-sm-6" >
                                                <div class="form-group row">
                                                    <lable class="col-sm-4"><strong>Ward</strong></lable>
                                                    <div class="col-xs-8 col-sm-8" >
                                                        <p>: {{$patients->ward_icu}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-5 col-sm-5" >
                                                <div class="form-group row">
                                                    <lable class="col-sm-4"><strong>BHT</strong></lable>
                                                    <div class="col-xs-8 col-sm-8" >
                                                        <p>: {{$patients->bht}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
        
                                        <div class="row">
                                            <div class="col-sm-1"></div>
                                            <div class="col-xs-6 col-sm-6" >
                                                <div class="form-group row">
                                                    <lable class="col-sm-4"><strong>Sex</strong></lable>
                                                    <div class="col-xs-8 col-sm-8" >
                                                        <p>: {{$patients->gender}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-5 col-sm-5" >
                                                <div class="form-group row">
                                                    <lable class="col-sm-4"><strong>Location</strong></lable>
                                                    <div class="col-xs-8 col-sm-8" >
                                                        <p>: {{$patients->location}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1"></div>
                                            <div class="col-xs-6 col-sm-6" style="padding-left: 0px;" >
                                                <div class="form-group">
                                                    <lable class="col-sm-4"><strong>Investigation</strong></lable>
                                                    <div class="col-xs-10 col-sm-10" >
                                                        <p id="p-invest">{{$patients->investigationr}}</p>
                                                         </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-5 col-sm-5" style="padding-left: 0px;"  >
                                                <div class="form-group">
                                                    <lable class="col-sm-4"><strong>Results</strong></lable>
                                                    <div class="col-xs-10 col-sm-10" >
                                                        <p id="p-result"><strong> {{$patients->resultsr}}</strong></p>
                                                       
                                                    </div>
                                                </div>
                                            
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-1"></div>
                                            <div class="col-xs-11 col-sm-11" style="padding-left: 0px;"  >
                                                <div class="form-group">
                                                    <lable class="col-sm-4"><strong>Comments</strong></lable>
                                                    <div class="col-xs-10 col-sm-10" >
                                                        <p id="p-comment">{{$patients->comments}}</p>
                                                             </div>
                                                </div>
                                            </div>
                                            
                                        </div>
        
                                           
                                        <div class="row col-sm-12" style="margin-top: 100px;">
                                            <div class="col-xs-4 col-sm-4" style="width: 100%" >
                                                <div class="form-group" style="text-align: center">
                                                   
                                                    <img src=" {{ asset('img/'.$patients->approval1by.'.png') }}" style="height: 80px"/>
                                                   
                                                    <br>
                                                    
                                                   <lable class="col-sm-12">...........................................</lable>
                                                    <br>
                                                    <lable class="col-sm-12">Medical Laboratory  Technologist</lable>
                                                     
                                                </div>
                                            </div>
                                            <div class="col-xs-4 col-sm-4" style="width: 100%"  >
                                                <div class="form-group" style="text-align: center">
                                                      
                                                    <img src=" {{ asset('img/'.$patients->approval2by.'.png') }}" style="height: 80px"/>
                                                   
                                                    <br>
                                                    <lable class="col-sm-12">...........................................</lable>
                                                     <br>
                                                     <lable class="col-sm-12">Reviewing MLT</lable>
                                                      
                                                 </div>
                                            
                                            </div>
                                            <div class="col-xs-4 col-sm-4" style="width: 100%"  >
                                                <div class="form-group" style="text-align: center">
                                                  
                                                    <img src=" {{ asset('img/'.$patients->approval3by.'.png') }}" style="height: 80px"/>
                                                   
                                                    <br>
                                                    <lable class="col-sm-12">...........................................</lable>
                                                     <br>
                                                     <lable class="col-sm-12">Consultant Microbiologist/Virologist</lable>
                                                      
                                                 </div>
                                            
                                            </div>
                                        </div>
                                       
                                </div>
                            </div>
                         </div>
                         @endforeach
                </div>
            </div>
        </div>
    </body>
</html>