@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div id="hideforprint">
                    <form method="post" action="/printlist" class="form-horizontal">
                                   
                        @csrf
                        
                     <div class="row" >
                        
                        <a href="importExportView" style="height: 50%; margin-left: 10px;" type="button" class="btn btn-primary" >{{ __('Import New Data Set') }}</a>
                       
                        
                      </div>     
                      <br>
                      <div class="row" style="margin-left: 10px">

                          
                        <label class="col-form-label">{{ __('From:') }}</label>
                        <div class="col-sm-2">
                            @if($request!="")
                            <input class="date form-control" value="{{$request->from_date}}" required name="from_date" id="from_date" type="date"  />
                           
                            @endif
                            @if($request=="")
                            <input class="date form-control"  required name="from_date" id="from_date" type="date"  />
                           
                            @endif

                            
                        </div>
                        <label class="col-form-label">{{ __('Location:') }}</label>
                        <div class="col-sm-5">
                            @if($request!="")
                            <input class="form-control" value="{{$request->location}}"  name="location" id="location" type="text"  />
                           
                            @endif
                            @if($request=="")
                            <input class="form-control"   name="location" id="location" type="text"  />
                           
                            @endif

                            
                        </div>

                       
                        
                      
                        <div class="col-sm-1">
                            <button type="submit" name="filter" class="btn btn-warning">Filter</button>
                        </div>
                        <div class="col-sm-1">
                            <button type="Button" onclick="hideforprint()" name="print" class="btn btn-primary">Print</button>
                        </div>
                        
                        
                      </div> 
                    </form>
                </div>
                      <br>
                      <div id="topic" class="row" style="margin-top: 0px; margin-bottom: 30px;">
                        <img src=" {{ asset('img/head.png') }}" style="height: 280px;width: 1800px;"/>
                    </div>
                      <div class="row" id="topic2" style="margin-left: 10px">
                          @php
                              $i=0;
                              $n=0;
                          @endphp
                        @foreach($patient as $patients)
                            @if($i==0)
                            @php
                              $i=1;
                            @endphp
                            <div class="col-md-6">
                            <p style="text-align: center"><strong>{{ $patients->investigationr}}</strong></p>
                            </div>
                            <div class="col-md-6">
                                </div>
                            <div class="col-md-12">
                                <p>Location: {{ $patients->send_location}}</p>
                            </div>
                            <div class="col-md-12">
                                <p>Sample type: {{ $patients->sample_type}}</p>
                            </div>
                            <div class="col-md-6">
                                <p>Recieve Date: {{ $patients->created_at}}</p>
                            </div>
                            <div class="col-md-6">
                                <p>Report Date: {{ $patients->report_date}}</p>
                            </div>
                            @endif
                        @endforeach
                      
                        
                      </div>  
                   
                      <table class="table table-striped table-bordered" style="width:100%" id="table">
                        <thead>
                            <tr>
                                <th class="text-center" style="max-width: 50px;padding:0; text-align: center">NCI Lab No</th>
                                <th class="text-center" style="max-width: 50px;padding:0; text-align: center">CCA No</th>
                                <th class="text-center" style="padding:0; text-align: center">S/N</th> 
                                <th class="text-center" style="padding:0; text-align: center">Name</th>
                                <th class="text-center" style="padding:0; text-align: center">Age</th>
                                <th class="text-center" style="padding:0; text-align: center">Gender</th>
                                
                               
                                <th class="text-center" style="padding:0; text-align: center">Results</th>
                               
                                
                            
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($patient as $patients)
                            <tr>
                                <td style="padding:0; text-align: center">{{$patients->laboratory_no}}</td>
                                <td style="padding:0; text-align: center">{{$patients->sample_no}}</td>
                                <td style="padding:0; text-align: center">{{++$n}}</td>
                                <td style="padding:0; text-align: center">{{ $patients->name}}</td>
                                <td style="padding:0; text-align: center">{{ $patients->age}}</td>
                                <td style="padding:0; text-align: center">{{ $patients->gender}}</td>
                               
                                <td style="padding:0; text-align: center">{{$patients->resultsr}}</td>
                             
                            </tr>
                           
                           
                            @endforeach
                        </tbody>
                    </table>
                    <div class="row">
                        @php
                        $j=0;
                    @endphp
                    
                        @foreach($patient as $patients)
                        @if($j==0)
                        @php
                          $j=1;
                        @endphp
                            <input type="hidden" name="id" value="{{$patients->id}}">
                            @if($patients->approval3 == "pending")
                            <div class="row">
                            <div class="col-sm-1"></div>
                        <div class="row col-sm-11" style="margin-top: 20px;">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                         <label class="form-check-label" for="exampleCheck1">I hereby authorize the results are true and correct </label>
                        </div>
                       
                            </div>
                           
                        @endif
                        <div class="row col-sm-12" style="margin-top: 100px;">
                            <div class="col-xs-4 col-sm-4" style="width: 100%" >
                                <div class="form-group" style="text-align: center">
                                    @if (Auth::user()->type=="MLT")
                                        @if($patients->approval1 == "pending")
                                            @if($patients->resultsr != "pending")
                                                
                                                <button type="submit" name="first" value="first" class="btn btn-primary">Sign</button>
                                            @endif
                                         @endif
                                    @endif
                                    @if($patients->approval1 != "pending" && $patients->approval1by != "")
                                        
                                    <img src=" {{ asset('img/'.$patients->approval1by.'.png') }}" style="height: 80px"/>
                                    @endif
                                    <br>
                                    
                                   <lable class="col-sm-12">...........................................</lable>
                                    <br>
                                    <lable class="col-sm-12">Medical Laboratory  Technologist</lable>
                                     
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-4" style="width: 100%"  >
                                <div class="form-group" style="text-align: center">
                                    @if (Auth::user()->type=="MLT")
                                        @if($patients->approval2 == "pending" && $patients->approval1 != "pending")
                                         @if($patients->approval1by != Auth::user()->id )
                                            <button type="submit" name="second" value="second" class="btn btn-primary">Sign</button>
                                            @endif
                                        @endif
                                    @endif
                                    @if($patients->approval2 != "pending" && $patients->approval2by != "")
                                        
                                    <img src=" {{ asset('img/'.$patients->approval2by.'.png') }}" style="height: 80px"/>
                                    @endif
                                    <br>
                                    <lable class="col-sm-12">...........................................</lable>
                                     <br>
                                     <lable class="col-sm-12">Reviewing MLT</lable>
                                      
                                 </div>
                            
                            </div>
                            <div class="col-xs-4 col-sm-4" style="width: 100%"  >
                                <div class="form-group" style="text-align: center">
                                    @if (Auth::user()->type=="Consultant")
                                        @if($patients->approval2 != "pending" && $patients->approval1 != "pending" && $patients->approval3 == "pending")
                                            <button type="submit" name="third" value="third" class="btn btn-primary">Sign</button>
                                        @endif
                                    @endif
                                    @if($patients->approval3 != "pending" && $patients->approval3by != "")
                                        
                                    <img src=" {{ asset('img/'.$patients->approval3by.'.png') }}" style="height: 80px"/>
                                    @endif
                                    <br>
                                    <lable class="col-sm-12">...........................................</lable>
                                     <br>
                                     <lable class="col-sm-12">Consultant Microbiologist/Virologist</lable>
                                     <div class="row col-sm-12" style="margin-bottom: 10px;">
                                        @if($patients->approval3 != "pending" && $patients->approval3by != "")
                                        @if($patients->approval3by == 4)
                                        <lable class="col-sm-12">For</lable>
                                    
                                        @endif
                                        <img style="float: right" width="300" height="80" src=" {{ asset('img/seal.png') }}" style="height: 80px"/>
                                        @endif
                                    </div>
                                 </div>
                            
                            </div>
                        </div>
                        @endif
                      @endforeach
                    
                    
                </div>


                </div>


            </div>
        </div>
    </div>
</div> 
       
<script>
    {{-- $('#lab').change(function() {
        alert($("#lab :selected").attr('value'))
    }); --}}

   $(document).ready(function() {
    $('#topic').hide();
      
   
  });

  function hideforprint(){
    $('#hideforprint').hide();
    $('#topic').show();
    $('footer').hide();
    window.print();
    
    setTimeout(function () { 
        $('#topic').hide();
        $('#hideforprint').show();
        $('footer').show();
    }, 100);
  }

 
   </script>
     



@endsection

