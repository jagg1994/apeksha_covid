@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
	
		<div class="col-md-offset-2 col-md-12 col-lg-offset-3 col-lg-12">
            @foreach($patient as $patients)
                 <div class="card">
                    <div class="card-body">
    	                <div class="well profile">
            
                            <div class="col-sm-12">
                                @if($patients->approval3 != "pending" && $patients->approval3by != "")

                                <button class="btn btn-primary" onclick="printing({{$patients->id}})" id="print">Print</button>
                                @endif
                                {{-- <div class="row" style="margin-top: 50px;">
                                    <div class="col-xs-2 col-sm-2" style="padding-left: 70px">
                                        <img src=" {{ asset('img/glogo.jpg') }}" style="height: 80px"/>
                                           
                                    </div>
                                    <div class="col-xs-10 col-sm-10" style="text-align: center">
                                       <h2>APEKSHA HOSPITAL(MAHARAGAMA)-SRILANKA</h2> 
                                       <h2>DEPARTMENT OF VIROLOGY</h2> 
                                    </div>
                                    <div class="col-sm-1"></div>
                                    <div class="col-xs-11 col-sm-11" style="text-align: center">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <p><strong>Telephone: 2844450</strong></p>
                                            </div>
                                            <div class="col-sm-3">
                                                <p><strong>Ex: 1248 </strong></p>
                                            </div>
                                            <div class="col-sm-3">
                                                <p><strong>Fax: 2842051</strong></p>
                                            </div>
                                            <div class="col-sm-3">
                                                <p><strong>Web: www.ncisl.lk</strong></p>
                                            </div>
                                        </div> 
                                     </div>
                                     <div class="col-sm-1"></div>

                                     <div class="col-xs-11 col-sm-11" style="text-align: center">
                                        <h3>CONFIDENTIAL LABORATORY REPORT</h3> 
                                       
                                     </div>
                                     <div class="col-xs-12 col-sm-12" style="text-align: center; margin-bottom: 20px;">
                                        <hr style="height:2px;border-width:0;color:gray;background-color:gray">
                                       
                                     </div>
                                        
                                
                                    </div> --}}
                                    <div class="row" style="margin-top: 0px; margin-bottom: 30px;">
                                        <img src=" {{ asset('img/head.png') }}" style="height: 280px;width: 1800px;"/>
                                    </div>
                                
                                    <form method="post" action="/resultsubmit" class="form-horizontal">
                                    @csrf
                                    
                                 <div class="row">
                                    <div class="col-sm-1"></div>
                                    <div class="col-xs-6 col-sm-6" >
                                        <div class="form-group row">
                                            <lable class="col-sm-4"><strong>NCI Lab No  </strong></lable>
                                            <div class="col-xs-8 col-sm-8" >
                                                <p>: {{$patients->laboratory_no}}</p>
                                            </div>
                                            
                                            <input type="hidden" class="form-control col-sm-6" name="id" value="{{$patients->id}}">
                                        </div>
                                        <div class="form-group row">
                                            <lable class="col-sm-4"><strong>CC Assigned No  </strong></lable>
                                            <div class="col-xs-8 col-sm-8" >
                                                <p>: {{$patients->sample_no}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 col-sm-5" >
                                        <div class="form-group row">
                                            <lable class="col-sm-4"><strong>Specimen </strong></lable>
                                            <div class="col-xs-8 col-sm-8" >
                                                <p id="p-type">: {{$patients->sample_type}}</p>
                                                <select id="type" style="display: none" class="form-control col-sm-10" name="specimen_type">
                                                    <option value="Nasopharyngeal & oropharyngeal swab" @if($patients->sample_type=="Nasopharyngeal & oropharyngeal swab") selected @endif>Nasopharyngeal & oropharyngeal swab</option>
                                                    <option value="sputum"  @if($patients->sample_type=="sputum") selected @endif>sputum</option>
                                                    <option value="Naso-pharyngeal Aspirate" @if($patients->sample_type=="Naso-pharyngeal Aspirate") selected @endif>Naso-pharyngeal Aspirate</option>
                                                    <option value="Tracheal Aspirate" @if($patients->sample_type=="Tracheal Aspirate") selected @endif>Tracheal Aspirate </option>
                                                    <option value="BAL" @if($patients->sample_type=="BAL") selected @endif>BAL</option>
                                                    <option value="Tissue Biopsy" @if($patients->sample_type=="Tissue Biopsy") selected @endif>Tissue Biopsy </option>
                                                   
                                                    <option value="Other" @if($patients->sample_type=="Other") selected @endif>Other</option>
                                                    
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <lable class="col-sm-4"><strong>Received Date  </strong></lable>
                                            <div class="col-xs-8 col-sm-8" >
                                                <p>: {{$patients->created_at}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="row">
                                    <div class="col-sm-1"></div>
                                    <div class="col-xs-6 col-sm-6" >
                                        <div class="form-group row">
                                            <lable class="col-sm-4"><strong>Name  </strong></lable>
                                            <div class="col-xs-8 col-sm-8" >
                                                <p id="p-name">: {{$patients->name}}</p>
                                                <input type="text" class="form-control" id="name" name="name" style="display: none" value="{{$patients->name}}"/>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 col-sm-5" >
                                        <div class="form-group row">
                                            <lable class="col-sm-4"><strong>Report Date  </strong></lable>
                                            <div class="col-xs-8 col-sm-8" >
                                                <p>: {{$patients->report_date}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-1"></div>
                                    <div class="col-xs-6 col-sm-6" >
                                        <div class="form-group row">
                                            <lable class="col-sm-4"><strong>Age</strong></lable>
                                            <div class="col-xs-8 col-sm-8" >
                                                <p id="p-age">: {{$patients->age}}</p>
                                                <input type="text" class="form-control" id="age" name="age" style="display: none" value="{{$patients->age}}"/>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 col-sm-5" >
                                        <div class="form-group row">
                                            <lable class="col-sm-4"><strong>BHT</strong></lable>
                                            <div class="col-xs-8 col-sm-8" >
                                                <p id="p-bht">: {{$patients->bht}}</p>
                                                <input type="text" class="form-control" id="bht" name="bht" style="display: none" value="{{$patients->bht}}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-1"></div>
                                    <div class="col-xs-6 col-sm-6" >
                                        <div class="form-group row">
                                            <lable class="col-sm-4"><strong>Ward</strong></lable>
                                            <div class="col-xs-8 col-sm-8" >
                                                <p>: {{$patients->ward_icu}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 col-sm-5" >
                                        <div class="form-group row">
                                            <lable class="col-sm-4"><strong>Location</strong></lable>
                                            <div class="col-xs-8 col-sm-8" >
                                                <p>: {{$patients->location}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-1"></div>
                                    <div class="col-xs-6 col-sm-6" >
                                        <div class="form-group row">
                                            <lable class="col-sm-4"><strong>Sex</strong></lable>
                                            <div class="col-xs-8 col-sm-8" >
                                                <p id="p-gender">: {{$patients->gender}}</p>
                                                <input type="text" class="form-control" id="gender" name="gender" style="display: none" value="{{$patients->gender}}"/>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 col-sm-5" >
                                       
                                    </div>
                                </div>
                                <div class="row" style="margin-top:50px;">
                                    <div class="col-sm-1"></div>
                                    <div class="col-xs-6 col-sm-6" style="padding-left: 0px;" >
                                        <div class="form-group">
                                            <lable class="col-sm-4"><strong>Investigation</strong></lable>
                                            <div class="col-xs-10 col-sm-10" >
                                                <p id="p-invest">{{$patients->investigationr}}</p>
                                                <input type="text" class="form-control" id="invest" name="investigation" style="display: none" value="SARS CoV-2 RNA Real Time RT-PCR"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 col-sm-5" style="padding-left: 0px;"  >
                                        <div class="form-group">
                                            <lable class="col-sm-4"><strong>Results</strong></lable>
                                            <div class="col-xs-10 col-sm-10" >
                                                <p id="p-result"><strong> {{$patients->resultsr}}</strong></p>
                                                <select id="result" style="display: none" class="form-control col-sm-10" name="result">
                                                    <option value="RNA Detected">RNA Detected</option>
                                                    <option value="RNA Not Detected">RNA Not Detected </option>
                                                    <option value="DNA Detected">DNA Detected</option>
                                                    <option value="Inconclusive">Inconclusive</option>
                                                                                                      
                                                </select>
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="row" style="margin-top:50px;">
                                    <div class="col-sm-1"></div>
                                    <div class="col-xs-11 col-sm-11" style="padding-left: 0px;"  >
                                        <div class="form-group">
                                            <lable class="col-sm-4"><strong>Comments</strong></lable>
                                            <div class="col-xs-10 col-sm-10" >
                                                <p id="p-comment">{{$patients->comments}}</p>
                                                <textarea style="display: none" id="comment"  class="form-control" rows="5" name="comments" >Please correlate with all clinical and epidemiological features. Suggest repeating with a lower respiratory sample if clinically indicated</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="row">
                                    <div class="col-xs-12 col-sm-12" >
                                        @if($patients->approval1 == "pending")
                                            <div class="col-xs-2 col-sm-2" style="float: right;" >
                                               <button id="change" type="button" class="btn btn-warning" onclick="editChange()">Edit Info</button>
                                               <button id="save" type="submit" class="btn btn-success" style="display: none" >Save</button>
                                               <button id="back" class="btn btn-primary" type="button" style="display: none" onclick="backChange()">Go back</button>
                                    
                                    </div>
                                    @endif
                                    
                                </div>


                            </div>
                                </form>
                               
                                <form method="post" action="/signReport" class="form-horizontal">
                                   
                                    @csrf
                                    <input type="hidden" name="id" value="{{$patients->id}}">
                                    @if($patients->approval3 == "pending")
                                    <div class="row">
                                    <div class="col-sm-1"></div>
                                <div class="row col-sm-11" style="margin-top: 20px;">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1" required>
                                 <label class="form-check-label" for="exampleCheck1">I hereby authorize the results are true and correct </label>
                                </div>
                               
                                    </div>
                                   
                                @endif
                                <div class="row col-sm-12" style="margin-top: 100px;">
                                    <div class="col-xs-4 col-sm-4" style="width: 100%" >
                                        <div class="form-group" style="text-align: center">
                                            @if (Auth::user()->type=="MLT")
                                                @if($patients->approval1 == "pending")
                                                    @if($patients->resultsr != "pending")
                                                        
                                                        <button type="submit" name="first" value="first" class="btn btn-primary">Sign</button>
                                                    @endif
                                                 @endif
                                            @endif
                                            @if($patients->approval1 != "pending" && $patients->approval1by != "")
                                                
                                            <img src=" {{ asset('img/'.$patients->approval1by.'.png') }}" style="height: 80px"/>
                                            @endif
                                            <br>
                                            
                                           <lable class="col-sm-12">...........................................</lable>
                                            <br>
                                            <lable class="col-sm-12">Medical Laboratory  Technologist</lable>
                                             
                                        </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4" style="width: 100%"  >
                                        <div class="form-group" style="text-align: center">
                                            @if (Auth::user()->type=="MLT")
                                                @if($patients->approval2 == "pending" && $patients->approval1 != "pending")
                                                 @if($patients->approval1by != Auth::user()->id )
                                                    <button type="submit" name="second" value="second" class="btn btn-primary">Sign</button>
                                                    @endif
                                                @endif
                                            @endif
                                            @if($patients->approval2 != "pending" && $patients->approval2by != "")
                                                
                                            <img src=" {{ asset('img/'.$patients->approval2by.'.png') }}" style="height: 80px"/>
                                            @endif
                                            <br>
                                            <lable class="col-sm-12">...........................................</lable>
                                             <br>
                                             <lable class="col-sm-12">Reviewing MLT</lable>
                                              
                                         </div>
                                    
                                    </div>
                                    <div class="col-xs-4 col-sm-4" style="width: 100%"  >
                                        <div class="form-group" style="text-align: center">
                                            @if (Auth::user()->type=="Consultant")
                                                @if($patients->approval2 != "pending" && $patients->approval1 != "pending" && $patients->approval3 == "pending")
                                                    <button type="submit" name="third" value="third" class="btn btn-primary">Sign</button>
                                                @endif
                                            @endif
                                            @if($patients->approval3 != "pending" && $patients->approval3by != "")
                                                
                                            <img src=" {{ asset('img/'.$patients->approval3by.'.png') }}" style="height: 80px"/>
                                            @endif
                                            <br>
                                            <lable class="col-sm-12">...........................................</lable>
                                             <br>
                                             <lable class="col-sm-12">Consultant Microbiologist/Virologist</lable>
                                             <div class="row col-sm-12" style="margin-bottom: 10px;">
                                                @if($patients->approval3 != "pending" && $patients->approval3by != "")
                                                @if($patients->approval3by == 4)
                                                <lable class="col-sm-12">For</lable>
                                            
                                                @endif
                                                {{-- <img style="float: right" width="300" height="80" src=" {{ asset('img/seal.png') }}" style="height: 80px"/> --}}
                                                @endif
                                            </div>
                                         </div>
                                    
                                    </div>
                                </div>
                                </form>
                        </div>
                    </div>
                 </div>
                 @endforeach
        </div>
    </div>
</div>
@endsection

<script>
    function editChange(){
        $('#change').hide();
        $('#save').show();
        $('#back').show();

        $('#type').show();
        $('#result').show();
        $('#comment').show();
        $('#invest').show();
        $('#name').show();
        $('#age').show();
        $('#gender').show();
        $('#bht').show();

        $('#p-type').hide();
        $('#p-result').hide();
        $('#p-comment').hide();
        $('#p-invest').hide();
        $('#p-gender').hide();
        $('#p-age').hide();
        $('#p-name').hide();
        $('#p-bht').hide();


    }
    function printing(id){
        $('#print').hide();
        $('footer').hide();
        window.print();
        window.location.href = "/savePrinted/"+id;
        setTimeout(function () { 
           
            $('#print').show();
            $('footer').show();
        }, 100);

        
    }
   
    function backChange(){
        $('#change').show();
        $('#save').hide();
        $('#back').hide();

        $('#type').hide();
        $('#result').hide();
        $('#comment').hide();
        $('#invest').hide();
        $('#name').hide();
        $('#age').hide();
        $('#gender').hide();
        $('#bht').hide();

        $('#p-type').show();
        $('#p-result').show();
        $('#p-comment').show();
        $('#p-invest').show();
        $('#p-gender').show();
        $('#p-age').show();
        $('#p-name').show();
        $('#p-bht').show();


    }

</script> 